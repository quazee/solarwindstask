#pragma once

#include "IReadStream.h"
#include <Windows.h>
#include <vector>
#include <stdexcept>

namespace win_api_except
{
	class WinException : public std::runtime_error
	{
	public:
		WinException(DWORD error, const std::string & msg);
		DWORD GetErrorCode() const;
	private:
		DWORD m_error;
	};

	std::string FormatErrorMessage(DWORD error, const std::string & msg);

	void ThrowLastErrorIf(bool expression, const std::string & msg);
}

namespace stream
{
	class ReadFileStream : public IReadStream
	{
	public:
		explicit ReadFileStream(const std::string & filename);
		~ReadFileStream();

		ReadFileStream(const ReadFileStream &) = delete;
		ReadFileStream & operator=(const ReadFileStream &) = delete;

	private:
		// Inherited via IReadStream
		std::vector<char> ReadImpl(std::size_t count) override;

	private:
		HANDLE m_file_handle;
		std::string m_filename;
	};
}