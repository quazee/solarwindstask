#include "IReadStream.h"

stream::IReadStream::Data::Data(const std::vector<char> & data) : m_data(data)
{}

std::vector<char> const & stream::IReadStream::Data::GetData() const
{
	return m_data;
}

stream::IReadStream::Data::Data(std::vector<char> && data) : m_data(data)
{}

stream::IReadStream::AsyncData::AsyncData(std::future<std::vector<char>> future) : m_future(future.share())
{}

std::vector<char> const & stream::IReadStream::AsyncData::GetData() const
{
	return m_future.get();
}

std::unique_ptr<stream::IData> stream::IReadStream::Read(std::size_t count)
{
	return std::make_unique<Data>(ReadSafe(count));
}

std::unique_ptr<stream::IData> stream::IReadStream::ReadAsync(std::size_t count)
{
	return std::make_unique<AsyncData>(std::async(std::launch::async, &IReadStream::ReadSafe, this, count));
}

std::vector<char> stream::IReadStream::ReadSafe(std::size_t count)
{
	std::lock_guard<std::mutex> lock(m_protect_parallel_read);
	return ReadImpl(count);
}
