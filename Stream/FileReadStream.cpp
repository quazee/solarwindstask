#include "FileReadStream.h"
#include <intsafe.h>

std::string win_api_except::FormatErrorMessage(DWORD error, const std::string & msg)
{
	static const int buffer_length = 1024;
	std::vector<char> buf(buffer_length);
	FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, 0, error, 0, buf.data(), buffer_length - 1, 0);
	return std::string(buf.data()) + " (" + msg + ")";
}

void win_api_except::ThrowLastErrorIf(bool expression, const std::string & msg)
{
	if (expression)
	{
		throw WinException(GetLastError(), msg);
	}
}

win_api_except::WinException::WinException(DWORD error, const std::string & msg)
	: runtime_error(FormatErrorMessage(error, msg))
	, m_error(error)
{}

DWORD win_api_except::WinException::GetErrorCode() const
{
	return m_error;
}

stream::ReadFileStream::ReadFileStream(const std::string & filename) : m_filename(filename)
{
	m_file_handle = CreateFileA(m_filename.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING,
								FILE_ATTRIBUTE_READONLY, nullptr);

	win_api_except::ThrowLastErrorIf(m_file_handle == INVALID_HANDLE_VALUE,
									 "Error in ReadFileStream::ctor while attempt to open file " + m_filename);
}

stream::ReadFileStream::~ReadFileStream()
{
	CloseHandle(m_file_handle);
}

std::vector<char> stream::ReadFileStream::ReadImpl(std::size_t count)
{
	DWORD bytes_read = 0;
	DWORD dword_count = 0;
	HRESULT cast_result = SizeTToDWord(count, &dword_count);
	win_api_except::ThrowLastErrorIf(cast_result != S_OK,
									 "ReadFileStream::ReadImpl integer cast overflow ");

	std::vector<char> readbuffer(dword_count);

	BOOL result = ReadFile(m_file_handle, readbuffer.data(), dword_count, &bytes_read, nullptr);
	win_api_except::ThrowLastErrorIf(result == FALSE,
									 "Error in ReadFileStream::ReadImpl while attempt to read file " + m_filename);

	if (bytes_read < count)
		readbuffer.resize(bytes_read);

	return readbuffer;
}
