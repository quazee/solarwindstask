#pragma once

#include <vector>
#include <future>

namespace stream
{
	class IData
	{
	public:
		// Can block thread if IData return from async operation
		virtual std::vector<char> const & GetData() const = 0;
		virtual ~IData() = default;
	};

	class IReadStream
	{
		class Data : public IData
		{
		public:
			Data(const std::vector<char> & data);
			Data(std::vector<char> && data);

			std::vector<char> const & GetData() const override;
		private:
			std::vector<char> m_data;
		};

		class AsyncData : public IData
		{
		public:
			AsyncData(std::future<std::vector<char>> future);

			std::vector<char> const & GetData() const override;
		private:
			std::shared_future<std::vector<char>> m_future;
		};

	public:
		std::unique_ptr<IData> Read(std::size_t count);
		std::unique_ptr<IData> ReadAsync(std::size_t count);

		virtual ~IReadStream() = default;

	private:
		std::vector<char> ReadSafe(std::size_t count);
		virtual std::vector<char> ReadImpl(std::size_t count) = 0;

	private:
		std::mutex m_protect_parallel_read;
	};
}