#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Stream/FileReadStream.h"
#include <Windows.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(ReadFileStream)
	{
	public:
		TEST_METHOD_INITIALIZE(TestDataInitialize)
		{
			char temp_file_name[MAX_PATH];
			char temp_env_path[MAX_PATH];

			//  Gets the temp path env string.
			DWORD dwRetVal = GetTempPathA(MAX_PATH, temp_env_path);
			if (dwRetVal > MAX_PATH || (dwRetVal == 0))
			{
				Assert::Fail(L"Can't GetTempPath", LINE_INFO());
			}
			//  Generates a temporary file name. 

			if (GetTempFileNameA(temp_env_path, TEXT("ReadAllFileData"), 0, temp_file_name) == 0)
			{
				Assert::Fail(L"Can't generate temp filename", LINE_INFO());
			}

			m_filename_with_data = temp_file_name;

			HANDLE handle_temp_file = CreateFileA((LPTSTR)temp_file_name, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS,
												  FILE_ATTRIBUTE_NORMAL, nullptr);

			if (handle_temp_file == INVALID_HANDLE_VALUE)
			{
				Assert::Fail(L"Can't create temp file", LINE_INFO());
			}

			m_filename_data = "Data for test, not quite good but need contrive some test";

			BOOL fSuccess = WriteFile(handle_temp_file, m_filename_data.data(),
									  static_cast<DWORD>(m_filename_data.size()), nullptr, nullptr);
			if (!fSuccess)
			{
				Assert::Fail(L"Can't write to temp file", LINE_INFO());
			}

			if (CloseHandle(handle_temp_file) == 0)
			{
				Assert::Fail(L"Can't close temp file", LINE_INFO());
			}

			//  Generates a temporary file name and delete file

			if (GetTempFileNameA(temp_env_path, TEXT("ReadAllFileData"), 0, temp_file_name) == 0)
			{
				Assert::Fail(L"Can't generate temp filename", LINE_INFO());
			}

			m_invalid_filename = temp_file_name;

			if (_unlink(temp_file_name) == -1)
			{
				Assert::Fail(L"Can't delete temp filename", LINE_INFO());
			}
		}

		TEST_METHOD_CLEANUP(TestDataCleanUp)
		{
			DeleteFileA(m_filename_with_data.data());
		}

		TEST_METHOD(ReadAllFileData)
		{
			stream::ReadFileStream testReadFileStream(m_filename_with_data);

			std::unique_ptr<stream::IData> data =
				testReadFileStream.Read(sizeof(std::string::value_type)*m_filename_data.size());

			Assert::AreEqual(data->GetData().size(), m_filename_data.size(), L"Read wrong count of bytes", LINE_INFO());
		}

		TEST_METHOD(ReadCorrectData)
		{
			stream::ReadFileStream testReadFileStream(m_filename_with_data);

			std::unique_ptr<stream::IData> data =
				testReadFileStream.Read(sizeof(std::string::value_type)*m_filename_data.size());
			for (size_t i = 0; i < m_filename_data.size(); ++i)
				Assert::AreEqual(data->GetData()[i], m_filename_data[i], L"Read incorrect data", LINE_INFO());
		}

		TEST_METHOD(ReadNonExistingFile)
		{
			auto create_rfs_on_invalid_filename = [this]()
			{
				stream::ReadFileStream rfs(m_invalid_filename);
			};

			Assert::ExpectException<win_api_except::WinException>(create_rfs_on_invalid_filename,
																  L"ReadFileStrean don't throw exception",
																  LINE_INFO());
		}

		TEST_METHOD(ReadAsyncAllFileData)
		{
			stream::ReadFileStream testReadFileStream(m_filename_with_data);

			std::unique_ptr<stream::IData> data =
				testReadFileStream.ReadAsync(sizeof(std::string::value_type)*m_filename_data.size());

			// Collecting possible exceptions
			data->GetData();

			Assert::AreEqual(data->GetData().size(), m_filename_data.size(), L"Read wrong count of bytes", LINE_INFO());
		}

		TEST_METHOD(ReadAsyncCheckMutex)
		{
			stream::ReadFileStream testReadFileStream(m_filename_with_data);

			std::unique_ptr<stream::IData> data =
				testReadFileStream.ReadAsync(sizeof(std::string::value_type)*m_filename_data.size());

			std::unique_ptr<stream::IData> data2 =
				testReadFileStream.ReadAsync(sizeof(std::string::value_type)*m_filename_data.size());

			// Collecting possible exceptions
			data->GetData();
			data2->GetData();

			Assert::AreEqual(data->GetData().size() + data2->GetData().size(), m_filename_data.size(), L"Incorrect read in diff thread", LINE_INFO());
		}

#ifdef _WIN64
		TEST_METHOD(ReadToManyBites)
		{
			stream::ReadFileStream testReadFileStream(m_filename_with_data);

			auto read_size_max_bytes = [&testReadFileStream]()
			{
				std::unique_ptr<stream::IData> data = testReadFileStream.Read(SIZE_MAX);
			};

			Assert::ExpectException<win_api_except::WinException>(read_size_max_bytes,
																  L"ReadFileStrean can't read SIZE_MAX on x64",
																  LINE_INFO());
		}
#endif

	private:
		std::string m_filename_with_data;
		std::string m_filename_data;
		std::string m_invalid_filename;
	};
}